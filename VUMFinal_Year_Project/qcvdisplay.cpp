#include "qcvdisplay.hpp"
#include <QPainter>
#include <opencv2/opencv.hpp>
#include <QApplication>
#include <QTransform>
#include <time.h>

QCVDisplay::QCVDisplay(QApplication* app, QWidget* parent)
	: QWidget(parent)
{
	m_capture = new cv::VideoCapture();
	m_coreApp = app;
	m_graphicsView.setScene(new QGraphicsScene(this));
	m_graphicsView.scene()->addItem(&m_pixmap);
	QImage tempImage = m_pixmap.pixmap().toImage();
	cv::Mat frame = cv::Mat::zeros(tempImage.height(), tempImage.width(), tempImage.format());

	QImage frameImage(frame.data,
				frame.cols,
				frame.rows,
				frame.step,
				QImage::Format_RGB888);
	frameImage.mirrored(false, true);

	m_pixmap.setPixmap(QPixmap::fromImage(frameImage.rgbSwapped()));
	m_graphicsView.fitInView(&m_pixmap, Qt::KeepAspectRatio);
}

void QCVDisplay::registerCallback(const std::function<void()>& callback)
{
	m_callbacks.append(callback);
}

const QGraphicsView* QCVDisplay::graphicsView() const
{ return &m_graphicsView; }

bool QCVDisplay::isOpened() const
{ return m_capture->isOpened(); }

void QCVDisplay::execute()
{
	if(!m_capture->isOpened())
		m_capture->open(0);

	while(m_capture->isOpened()) {
		cv::Mat frame;
		(*m_capture) >> frame;

		if(!frame.empty()) {

			for (auto func : m_callbacks) {
				func();
			}

			if(m_blackBackground) {

			}
			if(m_faceDetectorEnabled)
				frame = m_faceDetector.detect(frame);
			if(m_triangulationEnabled) {}
			if(m_voronoiDiagramEnabled) {}
			if(m_showFpsEnabled) {
				std::ostringstream strs;
				strs << m_capture->get(CV_CAP_PROP_FPS);
				cv::putText(frame, strs.str(), cv::Point(10, 25), cv::FONT_HERSHEY_PLAIN, 2, cv::Scalar(0, 255, 0),2, cv::LINE_AA);
			}
			if(m_setHeadTrackerEnabled) {}
			if(m_poseEstimatorEnabled)
				frame = m_openPoseEstimator.detect(frame);

			m_frame = frame;
			QImage frameImage(frame.data,
						frame.cols,
						frame.rows,
						frame.step,
						QImage::Format_RGB888);

			frameImage.mirrored(false, true);

			m_pixmap.setPixmap(QPixmap::fromImage(frameImage.rgbSwapped()));
			m_graphicsView.fitInView(&m_pixmap, Qt::KeepAspectRatio);

			Q_EMIT currentFrame(frame);
		}
		m_coreApp->processEvents();
	}
}

void QCVDisplay::close()
{
	m_capture->release();

	QImage tempImage = m_pixmap.pixmap().toImage();
	cv::Mat frame = cv::Mat::zeros(tempImage.height(), tempImage.width(), tempImage.format());

	QImage frameImage(frame.data,
				frame.cols,
				frame.rows,
				frame.step,
				QImage::Format_RGB888);
	frameImage.mirrored(false, true);

	m_pixmap.setPixmap(QPixmap::fromImage(frameImage.rgbSwapped()));
	m_graphicsView.fitInView(&m_pixmap, Qt::KeepAspectRatio);
}

QCVDisplay::~QCVDisplay()
{
	if(m_capture->isOpened()) {
		m_capture->release();
		delete m_capture;
	}
}

void QCVDisplay::saveScreenshot(const QString& filename)
{
	cv::imwrite(filename.toStdString(), m_frame);
}
