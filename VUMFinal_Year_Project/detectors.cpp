#include "detectors.hpp"

DEFINE_int32(logging_level, 3, "");
// Producer
DEFINE_string(image_path, "D:/Projects/people.jpg", "");
// OpenPose
DEFINE_string(model_pose, "BODY_25", "");
DEFINE_string(model_folder, "dependencies/", "");
DEFINE_string(net_resolution, "-1x112", "");
DEFINE_string(output_resolution, "-1x-1", ""
	" input image resolution.");
DEFINE_int32(num_gpu_start, 0, "");
DEFINE_double(scale_gap, 0.3, "");
DEFINE_int32(scale_number, 1, "");
// OpenPose Rendering
DEFINE_bool(disable_blending, false, "");
DEFINE_double(render_threshold, 0.05, "");
DEFINE_double(alpha_pose, 0.6, "");

OpenPoseEstimator::OpenPoseEstimator(QObject* parent)
	: AbstractDetector(parent)
{
	op::ConfigureLog::setPriorityThreshold((op::Priority)FLAGS_logging_level);
	const op::PoseModel poseModel = op::flagsToPoseModel(FLAGS_model_pose);

	scaleAndSizeExtractor = new op::ScaleAndSizeExtractor(
							op::flagsToPoint(FLAGS_net_resolution, "-1x368"),
							op::flagsToPoint(FLAGS_output_resolution, "-1x-1"),
							FLAGS_scale_number, FLAGS_scale_gap
				);

	cvMatToOpInput = new op::CvMatToOpInput(poseModel);
	poseExtractorCaffe = new op::PoseExtractorCaffe(poseModel,
													FLAGS_model_folder,
													FLAGS_num_gpu_start);

	poseRenderer = new op::PoseCpuRenderer(poseModel,
										   (float)FLAGS_render_threshold,
										   !FLAGS_disable_blending,
										   (float)FLAGS_alpha_pose);

	poseExtractorCaffe->initializationOnThread();
	poseRenderer->initializationOnThread();
	cv::setUseOptimized(true);
	cv::setNumThreads(4);
}

cv::Mat OpenPoseEstimator::detect(const cv::Mat& frame)
{
	const op::Point<int> imageSize{ frame.cols, frame.rows };

	std::vector<double> scaleInputToNetInputs;
	std::vector<op::Point<int>> netInputSizes;
	double scaleInputToOutput;
	op::Point<int> outputResolution;
	std::tie(scaleInputToNetInputs, netInputSizes, scaleInputToOutput, outputResolution) = scaleAndSizeExtractor->extract(imageSize);
	const auto netInputArray = cvMatToOpInput->createArray(frame, scaleInputToNetInputs, netInputSizes);
	auto outputArray = cvMatToOpOutput.createArray(frame, scaleInputToOutput, outputResolution);
	poseExtractorCaffe->forwardPass(netInputArray, imageSize, scaleInputToNetInputs);
	const auto poseKeypoints = poseExtractorCaffe->getPoseKeypoints();
	poseRenderer->renderPose(outputArray, poseKeypoints, scaleInputToOutput);
	return opOutputToCvMat.formatToCvMat(outputArray);
}

OpenPoseEstimator::~OpenPoseEstimator()
{
	delete poseRenderer;
	delete poseExtractorCaffe;
	delete cvMatToOpInput;
	delete scaleAndSizeExtractor;
}

CVPoseEstimator::CVPoseEstimator(QObject* parent)
	: AbstractDetector(parent)
{

}

cv::Mat CVPoseEstimator::detect(const cv::Mat& frame)
{
	return frame;
}

FaceDetector::FaceDetector(QObject* parent)
	: AbstractDetector(parent)
{
	detector = dlib::get_frontal_face_detector();
	dlib::deserialize("dependencies/shape_predictor_68_face_landmarks.dat") >> shapePredictor;
	faceCascade.load("dependencies/haarcascade_frontalface_alt2.xml");
	facemark = cv::face::FacemarkLBF::create();
	facemark->loadModel("dependencies/lbfmodel.yaml");
}

#define COLOR cv::Scalar(0, 200,0)

cv::Mat FaceDetector::detect(const cv::Mat& frame)
{
	dlib::cv_image<dlib::bgr_pixel> cimg(frame);

	// Detect faces
	std::vector<dlib::rectangle> faces = detector(cimg);
	// Find the pose of each face.
	std::vector<dlib::full_object_detection> shapes;
	for (unsigned long i = 0; i < faces.size(); ++i)
		shapes.push_back(shapePredictor(cimg, faces[i]));

	render_face_detections(shapes);
	if (!shapes.empty()) {
					int faceNumber = shapes.size();
					for (int j = 0; j < faceNumber; j++)
					{
						for (int i = 0; i < 68; i++)
						{
							cv::circle(frame, cvPoint(shapes[j].part(i).x(), shapes[j].part(i).y()), 3, cv::Scalar(0, 0, 255), -1);
						}
					}
				}

	return frame;
}
