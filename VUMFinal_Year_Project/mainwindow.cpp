#include "mainwindow.hpp"
#include "qcvdisplay.hpp"

#include <gflags/gflags.h>
// Allow Google Flags in Ubuntu 14
#ifndef GFLAGS_GFLAGS_H_
namespace gflags = google;
#endif
#include <opencv2/opencv.hpp>
#include <openpose/core/headers.hpp>
#include <openpose/filestream/headers.hpp>
#include <openpose/gui/headers.hpp>
#include <openpose/pose/headers.hpp>
#include <openpose/utilities/headers.hpp>
#include <QGraphicsScene>
#include <QApplication>
#include <QDockWidget>
#include <QPushButton>
#include <QDebug>
#include <opencv2/highgui.hpp>
#include <QGraphicsPixmapItem>
#include <QGraphicsView>
#include <QHBoxLayout>
#include "qcvdisplay.hpp"
#include "displayfilterwidget.hpp"
#include <QPlainTextEdit>
#include <QFileDialog>
#include "framesettingswidget.hpp"
#include <QToolBar>
#include <opencv2/highgui.hpp>
#include <QMenuBar>

MainWindow::MainWindow(QApplication* app, QWidget *parent)
	: QMainWindow(parent)
{
	m_display = new QCVDisplay(app, this);
	m_displayFilterWidget = new DisplayFilterWidget(this);
	m_textEdit = new QPlainTextEdit(this);
	m_frameSettingsWidget = new FrameSettingsWidget(this);
	m_textEdit->setReadOnly(true);
	m_menuBar = new QMenuBar(this);
	m_histogramsDockWidget = new QDockWidget("Histograms");

	m_toolBar = new QToolBar(this);
	m_toolBar->setMovable(false);
	m_toolBar->setFloatable(false);
	m_toolBar->addAction("Open Webcam");
	m_toolBar->addAction("Close Webcam");
	m_toolBar->addAction("Save Screenshot");
	m_toolBar->addSeparator();
	m_toolBar->addAction("Clear Log");
	m_toolBar->addAction("Reset Settings");

	addToolBar(m_toolBar);
	setMenuBar(m_menuBar);

	m_menuBar->addMenu("File");
	m_menuBar->addMenu("View");
	m_menuBar->addMenu("Settings");
	m_menuBar->addMenu("Help");

	m_displayFilterDockWidget = new QDockWidget("Display Filters", this);
	m_logDockWidget = new QDockWidget("Log", this);
	m_frameSettingsDockWidget = new QDockWidget("Frame Settings", this);
	m_filterDockWidget = new QDockWidget("Filters", this);

	m_displayFilterDockWidget->setWidget(m_displayFilterWidget);
	m_logDockWidget->setWidget(m_textEdit);
	m_frameSettingsDockWidget->setWidget(m_frameSettingsWidget);

	QObject::connect(m_displayFilterWidget, &DisplayFilterWidget::startCamClicked, m_display, &QCVDisplay::execute);
	QObject::connect(m_displayFilterWidget, &DisplayFilterWidget::stopCamClicked, m_display, &QCVDisplay::close);
	QObject::connect(m_displayFilterWidget, &DisplayFilterWidget::poseEstimatorCheckBoxClicked, m_display, &QCVDisplay::setPoseEstimatorEnabled);
	QObject::connect(m_displayFilterWidget, &DisplayFilterWidget::faceDetectorCheckBoxClicked, m_display, &QCVDisplay::setFaceDetectorEnabled);
	QObject::connect(m_displayFilterWidget, &DisplayFilterWidget::saveScreenshotButtonClicked, m_display, [this]() {
		QString filePath = QFileDialog::getSaveFileName(this);
		if(!filePath.isEmpty())
			m_display->saveScreenshot(filePath);
	});
	QObject::connect(m_displayFilterWidget, &DisplayFilterWidget::showFPSCheckBoxClicked, m_display, &QCVDisplay::setShowFPSEnabled);

	addDockWidget(Qt::DockWidgetArea::LeftDockWidgetArea, m_displayFilterDockWidget);
	addDockWidget(Qt::DockWidgetArea::LeftDockWidgetArea, m_logDockWidget);
	addDockWidget(Qt::DockWidgetArea::BottomDockWidgetArea, m_frameSettingsDockWidget);
	addDockWidget(Qt::DockWidgetArea::RightDockWidgetArea, m_histogramsDockWidget);
	addDockWidget(Qt::DockWidgetArea::RightDockWidgetArea, m_filterDockWidget);

	setMinimumSize(1280, 800);
	setCentralWidget(const_cast<QGraphicsView*>(m_display->graphicsView()));
}

void MainWindow::closeEvent(QCloseEvent* /* ev */)
{
	if(m_display->isOpened())
		m_display->close();
}

void MainWindow::showEvent(QShowEvent* ev)
{
	QMainWindow::showEvent(ev);
}

MainWindow::~MainWindow()
{
}
