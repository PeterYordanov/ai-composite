#include "framesettingswidget.hpp"
#include <QLabel>

#define SETUP_SLIDER(x) \
	x.setRange(0, 100); \
	x.setOrientation(Qt::Orientation::Horizontal);\
	x.setTickInterval(5);\
	x.setTickPosition(QSlider::TicksBelow)

FrameSettingsWidget::FrameSettingsWidget(QWidget* parent)
	: QWidget(parent)
{
	SETUP_SLIDER(m_contrastSlider);
	SETUP_SLIDER(m_zoomSlider);
	SETUP_SLIDER(m_blurSlider);
	SETUP_SLIDER(m_smoothSlider);
	SETUP_SLIDER(m_brightnessSlider);

	m_settingsLayout.addWidget(new QLabel("Zoom", this));
	m_settingsLayout.addWidget(&m_zoomSlider);
	m_settingsLayout.addWidget(new QLabel("Contrast", this));
	m_settingsLayout.addWidget(&m_contrastSlider);
	m_settingsLayout.addWidget(new QLabel("Brightness", this));
	m_settingsLayout.addWidget(&m_brightnessSlider);
	m_settingsLayout.addWidget(new QLabel("Blur", this));
	m_settingsLayout.addWidget(&m_blurSlider);
	m_settingsLayout.addWidget(new QLabel("Smooth", this));
	m_settingsLayout.addWidget(&m_smoothSlider);

	setLayout(&m_settingsLayout);
}
