#ifndef MISC_HPP
#define MISC_HPP

#define VPE_LINE __LINE__
#define VPE_FUNCTION __FUNCTION__
#define VPE_FILE __FILE__

#define VPE_NO_COPY
#define VPE_NO_MOVE

#define VPE_DEFAULT_CAMERA_INDEX 0

#endif // MISC_HPP
