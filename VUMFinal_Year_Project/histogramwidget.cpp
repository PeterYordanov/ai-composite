#include "histogramwidget.hpp"

HistogramWidget::HistogramWidget(QWidget* parent)
	: QTabBar(parent)
{
	addTab("Histogram 1");
	addTab("Histogram 2");
	addTab("Histogram 3");
	addTab("Histogram 4");
}
