#ifndef FRAMESETTINGS_HPP
#define FRAMESETTINGS_HPP

#include <QWidget>
#include <QDial>
#include <QSlider>
#include <QVBoxLayout>
#include <QHBoxLayout>

class FrameSettingsWidget : public QWidget
{
	Q_OBJECT
public:
	explicit FrameSettingsWidget(QWidget* parent = Q_NULLPTR);

private:
	QVBoxLayout m_settingsLayout;
	QHBoxLayout m_dials;
	QSlider m_contrastSlider;
	QSlider m_brightnessSlider;
	QSlider m_zoomSlider;
	QSlider m_blurSlider;
	QSlider m_smoothSlider;

	QDial m_rotateDial;
	QDial m_shearDial;
};

#endif // FRAMESETTINGS_HPP
