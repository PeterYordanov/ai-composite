#ifndef DETECTORS_HPP
#define DETECTORS_HPP

#include "abstractdetector.hpp"
#include <gflags/gflags.h>
#include <openpose/core/headers.hpp>
#include <openpose/filestream/headers.hpp>
#include <openpose/gui/headers.hpp>
#include <openpose/pose/headers.hpp>
#include <openpose/utilities/headers.hpp>
#ifndef GFLAGS_GFLAGS_H_
namespace gflags = google;
#endif
#include <dlib/opencv/cv_image.h>
#include <dlib/opencv.h>
#include <opencv2/opencv.hpp>
#include <opencv2/face.hpp>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>
#include <opencv2/opencv.hpp>

class FaceDetector : public AbstractDetector
{
public:
	explicit FaceDetector(QObject* parent = Q_NULLPTR);
	cv::Mat detect(const cv::Mat&);
	Q_NEVER_INLINE QString type() const
	{ return "CV Face Detector"; }

private:
	dlib::frontal_face_detector detector;
	dlib::shape_predictor shapePredictor;
	cv::CascadeClassifier faceCascade;
	cv::Ptr<cv::face::Facemark> facemark;
};

class OpenPoseEstimator : public AbstractDetector
{
public:
	explicit OpenPoseEstimator(QObject* parent = Q_NULLPTR);
	~OpenPoseEstimator();
	cv::Mat detect(const cv::Mat&);
	Q_NEVER_INLINE QString type() const
	{ return "OpenPose"; }

private:
	op::CvMatToOpInput*			cvMatToOpInput;
	op::PoseExtractorCaffe*		poseExtractorCaffe;
	op::PoseCpuRenderer*		poseRenderer;
	op::CvMatToOpOutput			cvMatToOpOutput;
	op::ScaleAndSizeExtractor*  scaleAndSizeExtractor;
	op::OpOutputToCvMat			opOutputToCvMat;
};

class CVPoseEstimator : public AbstractDetector
{
	explicit CVPoseEstimator(QObject* parent = Q_NULLPTR);
	cv::Mat detect(const cv::Mat&);
	Q_NEVER_INLINE QString type() const
	{ return "CV Pose Estimator"; }
};



#endif // DETECTORS_HPP
