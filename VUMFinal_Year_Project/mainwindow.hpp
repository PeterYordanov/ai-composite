#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>

class QCVDisplay;
class DisplayFilterWidget;
class QDockWidget;
class QHBoxLayout;
class QPlainTextEdit;
class FrameSettingsWidget;
class QMenu;
class QToolBar;
class QMenuBar;
class FilterSettingsWidget;

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QApplication* app, QWidget* parent = Q_NULLPTR);
	~MainWindow();

	virtual void showEvent(QShowEvent*);

public Q_SLOTS:
	void closeEvent(QCloseEvent*);

private:	
	QDockWidget*		 m_frameSettingsDockWidget;
	QDockWidget*		 m_displayFilterDockWidget;
	QDockWidget*		 m_logDockWidget;
	QDockWidget*		 m_histogramsDockWidget;
	QDockWidget*		 m_filterDockWidget;

	QCVDisplay*			 m_display;
	DisplayFilterWidget* m_displayFilterWidget;
	FrameSettingsWidget* m_frameSettingsWidget;

	QPlainTextEdit*		 m_textEdit;

	QToolBar* m_toolBar;
	QMenuBar* m_menuBar;
};

#endif // MAINWINDOW_HPP
