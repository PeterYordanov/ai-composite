#include "displayfilterwidget.hpp"

DisplayFilterWidget::DisplayFilterWidget(QWidget* parent)
	: QWidget(parent)
{
	m_poseEstimatorCheckBox.setText("Pose Estimation");
	m_faceDetectorCheckBox.setText("Face Detection");
	m_headTrackerCheckBox.setText("Head Tracking");
	m_voronoiDiagramCheckBox.setText("Voronoi Diagram");
	m_triangulationCheckBox.setText("Triangulate");
	m_showFPSCheckBox.setText("Show FPS");
	m_startCamButton.setText("Start Webcam");
	m_stopCamButton.setText("Stop Webcam");
	m_saveScreenshotButton.setText("Save Screenshot");

	m_checkBoxesLayout.addWidget(&m_poseEstimatorCheckBox);
	m_checkBoxesLayout.addWidget(&m_faceDetectorCheckBox);
	m_checkBoxesLayout.addWidget(&m_headTrackerCheckBox);
	m_checkBoxesLayout.addWidget(&m_voronoiDiagramCheckBox);
	m_checkBoxesLayout.addWidget(&m_showFPSCheckBox);
	m_checkBoxesLayout.addWidget(&m_triangulationCheckBox);

	m_buttonLayout.addWidget(&m_startCamButton);
	m_buttonLayout.addWidget(&m_stopCamButton);
	m_buttonLayout.addWidget(&m_saveScreenshotButton);

	m_vboxLayout.addLayout(&m_checkBoxesLayout);
	m_vboxLayout.addLayout(&m_buttonLayout);
	setLayout(&m_vboxLayout);

	QObject::connect(&m_poseEstimatorCheckBox, &QCheckBox::clicked, this, [this](bool clicked) {
		Q_EMIT poseEstimatorCheckBoxClicked(clicked);
	});

	QObject::connect(&m_startCamButton, &QPushButton::clicked, this, [this](bool clicked) {
		Q_EMIT startCamClicked(clicked);
	});

	QObject::connect(&m_stopCamButton, &QPushButton::clicked, this, [this](bool clicked) {
		Q_EMIT stopCamClicked(clicked);
	});

	QObject::connect(&m_saveScreenshotButton, &QPushButton::clicked, this, [this](bool clicked) {
		Q_EMIT saveScreenshotButtonClicked(clicked);
	});

	QObject::connect(&m_faceDetectorCheckBox, &QPushButton::clicked, this, [this](bool clicked) {
		Q_EMIT faceDetectorCheckBoxClicked(clicked);
	});

	QObject::connect(&m_showFPSCheckBox, &QPushButton::clicked, this, [this](bool clicked) {
		Q_EMIT showFPSCheckBoxClicked(clicked);
	});
}
