#ifndef ABSTRACTDETECTOR_HPP
#define ABSTRACTDETECTOR_HPP

#include <QObject>
#include <opencv2/core/mat.hpp>
class QString;

class AbstractDetector : public QObject
{
public:
	explicit AbstractDetector(QObject* parent = Q_NULLPTR);
	virtual cv::Mat detect(const cv::Mat&) = 0;
	virtual QString type() const = 0;
};

#endif // ABSTRACTDETECTOR_HPP
