#-------------------------------------------------
#
# Project created by QtCreator 2019-06-17T01:35:03
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VUMFinal_Year_Project
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++17

SOURCES += \
        abstractdetector.cpp \
        detectors.cpp \
        displayfilterwidget.cpp \
        framesettingswidget.cpp \
        histogramwidget.cpp \
        main.cpp \
        mainwindow.cpp \
        math.cpp \
        qcvdisplay.cpp

HEADERS += \
        abstractdetector.hpp \
        detectors.hpp \
        displayfilterwidget.hpp \
        framesettingswidget.hpp \
        histogramwidget.hpp \
        mainwindow.hpp \
        math.hpp \
        misc.hpp \
        qcvdisplay.hpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32: LIBS += -L$$PWD/3rdparty/openpose/ -lopenpose

INCLUDEPATH += $$PWD/3rdparty/openpose/include
DEPENDPATH += $$PWD/3rdparty/openpose/include

win32: LIBS += -L$$PWD/3rdparty/PrebuildOpenCV40/x64/vc15/lib/ -lopencv_world400

INCLUDEPATH += $$PWD/3rdparty/PrebuildOpenCV40/include
DEPENDPATH += $$PWD/3rdparty/PrebuildOpenCV40/include

win32: LIBS += -L$$PWD/3rdparty/PrebuildOpenCV40/x64/vc15/lib/ -lopencv_img_hash400

INCLUDEPATH += $$PWD/3rdparty/PrebuildOpenCV40/include
DEPENDPATH += $$PWD/3rdparty/PrebuildOpenCV40/include

win32: LIBS += -L$$PWD/3rdparty/windows/caffe3rdparty/lib/ -lgflags

INCLUDEPATH += $$PWD/3rdparty/windows/caffe3rdparty/include
DEPENDPATH += $$PWD/3rdparty/windows/caffe3rdparty/include

win32: LIBS += -L$$PWD/3rdparty/windows/caffe3rdparty/lib/ -lglog

INCLUDEPATH += $$PWD/3rdparty/windows/caffe3rdparty/include
DEPENDPATH += $$PWD/3rdparty/windows/caffe3rdparty/include

win32: LIBS += -L$$PWD/3rdparty/windows/caffe/lib/ -lcaffe
win32: LIBS += -L$$PWD/3rdparty/windows/caffe/lib/ -lcaffeproto

INCLUDEPATH += $$PWD/3rdparty/windows/caffe/include
DEPENDPATH += $$PWD/3rdparty/windows/caffe/include

INCLUDEPATH += $$PWD/3rdparty/windows/caffe/include2
DEPENDPATH += $$PWD/3rdparty/windows/caffe/include2

win32: LIBS += -L'C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v10.1/lib/x64/' -lcudart_static

INCLUDEPATH += 'C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v10.1/include'
DEPENDPATH += 'C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v10.1/include'

win32: LIBS += -L$$PWD/3rdparty/dlib_x64-windows/lib/ -ldlib19.17.0_release_64bit_msvc1921

INCLUDEPATH += $$PWD/3rdparty/dlib_x64-windows/include
DEPENDPATH += $$PWD/3rdparty/dlib_x64-windows/include

win32: LIBS += -L$$PWD/3rdparty/openblas_x64-windows/lib/ -lopenblas

INCLUDEPATH += $$PWD/3rdparty/openblas_x64-windows/include
DEPENDPATH += $$PWD/3rdparty/openblas_x64-windows/include

win32: LIBS += -L$$PWD/3rdparty/clapack_x64-windows/lib/ -llapack

INCLUDEPATH += $$PWD/3rdparty/clapack_x64-windows/include
DEPENDPATH += $$PWD/3rdparty/clapack_x64-windows/include

win32: LIBS += -L$$PWD/3rdparty/clapack_x64-windows/lib/ -llibf2c

INCLUDEPATH += $$PWD/3rdparty/clapack_x64-windows/include
DEPENDPATH += $$PWD/3rdparty/clapack_x64-windows/include
