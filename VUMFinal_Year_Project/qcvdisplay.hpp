#ifndef QCVDISPLAY_HPP
#define QCVDISPLAY_HPP

#include <QWidget>
#include <opencv2/highgui.hpp>
#include <functional>
#include <QVector>
#include <QGraphicsPixmapItem>
#include <QGraphicsView>
#include <QGraphicsScene>
#include "detectors.hpp"

class QCVDisplay : public QWidget
 {
	Q_OBJECT
public:
	explicit QCVDisplay(QApplication* app, QWidget* parent = Q_NULLPTR);
	~QCVDisplay();

	void registerCallback(const std::function<void()>& callback);
	const QGraphicsView* graphicsView() const;
	bool isOpened() const;

public Q_SLOTS:
	void execute();
	void close();

	void saveScreenshot(const QString&);

	inline void setPoseEstimatorEnabled(bool enable)
	{ m_poseEstimatorEnabled = enable; }
	inline void setFaceDetectorEnabled(bool enable)
	{ m_faceDetectorEnabled = enable; }
	inline void setTriangulationEnabled(bool enable)
	{ m_triangulationEnabled = enable; }
	inline void setVoronoiDiagramEnabled(bool enable)
	{ m_voronoiDiagramEnabled = enable; }
	inline void setShowFPSEnabled(bool enable)
	{ m_showFpsEnabled = enable; }
	inline void setHeadTrackerEnabled(bool enable)
	{ m_setHeadTrackerEnabled = enable; }

	inline void setBlackBackground(bool enable)
	{ m_blackBackground = enable; }

Q_SIGNALS:
	void currentFrame(const cv::Mat&);
	void cameraOpened(bool);

private:
	QVector<std::function<void()>> m_callbacks;
	cv::VideoCapture* m_capture;
	QGraphicsPixmapItem m_pixmap;
	QGraphicsView m_graphicsView;
	QApplication* m_coreApp;
	cv::Mat		  m_frame;

	OpenPoseEstimator m_openPoseEstimator;
	FaceDetector m_faceDetector;

	bool m_poseEstimatorEnabled = false;
	bool m_faceDetectorEnabled = false;
	bool m_triangulationEnabled = false;
	bool m_voronoiDiagramEnabled = false;
	bool m_showFpsEnabled = false;
	bool m_setHeadTrackerEnabled = false;
	bool m_saveScreenshot = false;

	bool m_blackBackground = false;

	int rows = 0;
	int cols = 0;
	int type = 0;
};

#endif // QCVDISPLAY_HPP
