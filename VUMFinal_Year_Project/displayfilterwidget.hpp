#ifndef DISPLAYFILTERWIDGET_HPP
#define DISPLAYFILTERWIDGET_HPP

#include <QWidget>
#include <QVBoxLayout>
#include <QCheckBox>
#include <QBoxLayout>
#include <QPushButton>

class DisplayFilterWidget : public QWidget
{
	Q_OBJECT

public:
	explicit DisplayFilterWidget(QWidget* parent = Q_NULLPTR);

Q_SIGNALS:
	void poseEstimatorCheckBoxClicked(bool);
	void faceDetectorCheckBoxClicked(bool);
	void triangulationCheckBoxClicked(bool);
	void voronoiDiagramCheckBoxClicked(bool);
	void showFPSCheckBoxClicked(bool);
	void headTrackerCheckBoxClicked(bool);
	void startCamClicked(bool);
	void stopCamClicked(bool);
	void saveScreenshotButtonClicked(bool);

private:
	QVBoxLayout m_vboxLayout;

	QCheckBox m_poseEstimatorCheckBox;
	QCheckBox m_faceDetectorCheckBox;
	QCheckBox m_triangulationCheckBox;
	QCheckBox m_voronoiDiagramCheckBox;
	QCheckBox m_showFPSCheckBox;
	QCheckBox m_headTrackerCheckBox;
	QVBoxLayout m_checkBoxesLayout;

	QPushButton m_startCamButton;
	QPushButton m_stopCamButton;
	QPushButton m_saveScreenshotButton;
	QHBoxLayout m_buttonLayout;
};

#endif // DISPLAYFILTERWIDGET_HPP
