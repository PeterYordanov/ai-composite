#include "mainwindow.hpp"
#include <QApplication>

/*
Check AVX2
Check AVX512
Check SSE2
Check SSE4.1
Check SSE4.2
Check OpenCL 1.2
Check CUDA 10.1
Check RAM
Check CPU
*/
int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow w(&a);
	w.show();

	return a.exec();
}
