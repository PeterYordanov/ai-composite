#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "dlib::dlib" for configuration "Release"
set_property(TARGET dlib::dlib APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(dlib::dlib PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v10.1/lib/x64/cudart_static.lib;ws2_32;winmm;comctl32;gdi32;imm32;${_IMPORT_PREFIX}/lib/libpng16.lib;${_IMPORT_PREFIX}/lib/zlib.lib;${_IMPORT_PREFIX}/lib/openblas.lib;${_IMPORT_PREFIX}/lib/lapack.lib;${_IMPORT_PREFIX}/lib/libf2c.lib;${_IMPORT_PREFIX}/lib/openblas.lib;C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v10.1/lib/x64/cublas.lib;C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v10.1/lib/x64/cudnn.lib;C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v10.1/lib/x64/curand.lib;C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v10.1/lib/x64/cusolver.lib;sqlite3;FFTW3::fftw3"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/dlib19.17.0_release_64bit_msvc1921.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS dlib::dlib )
list(APPEND _IMPORT_CHECK_FILES_FOR_dlib::dlib "${_IMPORT_PREFIX}/lib/dlib19.17.0_release_64bit_msvc1921.lib" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
