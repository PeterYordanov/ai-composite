#----------------------------------------------------------------
# Generated CMake target import file for configuration "Debug".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "dlib::dlib" for configuration "Debug"
set_property(TARGET dlib::dlib APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(dlib::dlib PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_DEBUG "C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v10.1/lib/x64/cudart_static.lib;ws2_32;winmm;comctl32;gdi32;imm32;${_IMPORT_PREFIX}/debug/lib/libpng16d.lib;${_IMPORT_PREFIX}/debug/lib/zlibd.lib;${_IMPORT_PREFIX}/debug/lib/openblas.lib;${_IMPORT_PREFIX}/debug/lib/lapackd.lib;${_IMPORT_PREFIX}/debug/lib/libf2cd.lib;${_IMPORT_PREFIX}/debug/lib/openblas.lib;C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v10.1/lib/x64/cublas.lib;C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v10.1/lib/x64/cudnn.lib;C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v10.1/lib/x64/curand.lib;C:/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v10.1/lib/x64/cusolver.lib;sqlite3;FFTW3::fftw3"
  IMPORTED_LOCATION_DEBUG "${_IMPORT_PREFIX}/debug/lib/dlib19.17.0_debug_64bit_msvc1921.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS dlib::dlib )
list(APPEND _IMPORT_CHECK_FILES_FOR_dlib::dlib "${_IMPORT_PREFIX}/debug/lib/dlib19.17.0_debug_64bit_msvc1921.lib" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
